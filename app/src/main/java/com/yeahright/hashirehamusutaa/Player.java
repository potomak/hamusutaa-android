package com.yeahright.hashirehamusutaa;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class Player {
    public final String id;
    public final String name;
    public final String color;
    public final int price;
    public boolean isMoving;
    public int totalSteps;
    public int dailySteps;
    public int balance;

    public Player(String id, String name, String color, int price, boolean isMoving, int totalSteps, int dailySteps) {
        this(id, name, color, price, isMoving, totalSteps, dailySteps, 0);
    }

    public Player(String id, String name, String color, int price, boolean isMoving, int totalSteps, int dailySteps, int balance) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.price = price;
        this.isMoving = isMoving;
        this.totalSteps = totalSteps;
        this.dailySteps = dailySteps;
        this.balance = balance;
    }

    public Player(JSONObject object) throws JSONException {
        id = object.getString("id");
        name = object.getString("name");
        color = object.getString("color");
        price = object.getInt("price");
        isMoving = object.getBoolean("isMoving");
        totalSteps = object.getInt("totalSteps");
        dailySteps = object.getInt("dailySteps");
        if (object.has("balance")) {
            balance = object.getInt("balance");
        } else {
            balance = 0;
        }
    }
}
