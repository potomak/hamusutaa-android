package com.yeahright.hashirehamusutaa;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.msebera.android.httpclient.Header;

public class StoreActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "StoreActivity";

    private StoreItemAdapter mListAdapter;
    private String mPlayerId;
    private int mBalance;
    private Typeface billy;
    private Set<String> mPlayerIds;
    private ProgressBar mProgressBar;
    private GridView mGridView;
    private TextView mStoreEmptyTextView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        mGridView = (GridView)findViewById(R.id.gridView);

        Intent intent = getIntent();
        mPlayerId = intent.getStringExtra(HamusutaaPreferences.PLAYER_ID);
        final String token = intent.getStringExtra(HamusutaaPreferences.TOKEN);
        final String gcmToken = intent.getStringExtra(HamusutaaPreferences.GCM_TOKEN);
        mPlayerIds = new HashSet<>(intent.getStringArrayListExtra(HamusutaaPreferences.PLAYER_IDS));
        mBalance = intent.getIntExtra(HamusutaaPreferences.BALANCE, 0);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mStoreEmptyTextView = (TextView) findViewById(R.id.storeEmptyTextView);
        billy = Typeface.createFromAsset(getAssets(), "billy.ttf");
        mStoreEmptyTextView.setTypeface(billy);

        fetchStoreItems();

        mListAdapter = new StoreItemAdapter(getApplicationContext());
        mGridView.setAdapter(mListAdapter);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final String playerId = ((StoreItem) mListAdapter.getItem(position)).player.id;
                HamusutaaClient.postBuy(playerId, token, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Log.i(TAG, "Player " + playerId + " bought successfully");

                        StoreItem storeItem = mListAdapter.getStoreItemById(playerId);
                        if (storeItem == null) {
                            Log.e(TAG, "Trying to buy player " + playerId + " that is not present");
                            return;
                        }

                        mListAdapter.handleNewTransaction(playerId);

                        // Subscribe to player's topic
                        new SubscribeToTopic(getApplicationContext(), gcmToken, playerId).execute();

                        DecimalFormat decimalFormat = new DecimalFormat();
                        String currency = getString(R.string.currency);
                        String message = getString(R.string.transaction_success);
                        String compiled_message = String.format(message, storeItem.player.name, decimalFormat.format(storeItem.player.price), currency);
                        Toast.makeText(getApplicationContext(), compiled_message, Toast.LENGTH_SHORT).show();

                        // Update local balance
                        mBalance -= storeItem.player.price;
                        // Hack bonus event to notify balance's value change
                        Intent intent = new Intent(HamusutaaGcmListenerService.BONUS_EVENT);
                        intent.putExtra(HamusutaaPreferences.BONUS_AMOUNT, -storeItem.player.price);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.e(TAG, "Error trying to buy player " + playerId, error);
                        String errorMessage;
                        if (responseBody != null) {
                            errorMessage = new String(responseBody);
                        } else {
                            errorMessage = error.getMessage();
                        }
                        Log.d(TAG, "Error message: " + errorMessage);
                        Toast.makeText(getApplicationContext(), errorMessage.trim(), Toast.LENGTH_SHORT).show();

                        // Remove stored preferences and open registration intent if the token is not valid anymore
                        if (statusCode == 401) {
                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().apply();

                            Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
                            startActivity(registerIntent);
                            finish();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        fetchStoreItems();
    }

    private void fetchStoreItems() {
        HamusutaaClient.getPlayers(mBalance, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray responseBody) {
                Log.i(TAG, "Got players: " + responseBody.toString());
                List<StoreItem> storeItems = new ArrayList<>();
                Map<String, Integer> storeItemsPositions = new HashMap<>();
                int j = 0;
                for (int i = 0; i < responseBody.length(); i++) {
                    Player player = null;
                    try {
                        player = new Player(responseBody.getJSONObject(i));
                    } catch (JSONException e) {
                        Log.e(TAG, "Error creating store items list", e);
                    }

                    // Show players that are not the current player and that are not included in
                    // the list of purchased players
                    if (!player.id.equals(mPlayerId) && !mPlayerIds.contains(player.id)) {
                        Log.d(TAG, "Adding player " + player.name + " (" + player.id + ")");
                        storeItems.add(j, new StoreItem(player));
                        storeItemsPositions.put(player.id, j);
                        j++;
                    }
                }

                Log.d(TAG, "Store items: " + Arrays.toString(storeItems.toArray()));
                Log.d(TAG, "Store items-positions: " + Arrays.toString(storeItemsPositions.entrySet().toArray()));
                Log.d(TAG, "Current player's balance: " + mBalance + " " + getString(R.string.currency));
                mListAdapter.loadStoreItems(storeItems, storeItemsPositions);

                // Show intro if there are no store items
                if (storeItems.size() == 0) {
                    mStoreEmptyTextView.setVisibility(View.VISIBLE);
                } else {
                    mStoreEmptyTextView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseBody) {
                Log.e(TAG, "GET /players error: " + statusCode + ", body: " + responseBody, error);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                Log.e(TAG, "GET /players error: " + statusCode + ", body: " + responseBody, error);

                // Remove stored preferences and open registration intent if the token is not valid anymore
                if (statusCode == 401) {
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().apply();

                    Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
                    startActivity(registerIntent);
                    finish();
                }
            }

            @Override
            public void onFinish() {
                mProgressBar.setVisibility(ProgressBar.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private static class StoreItem {
        public final Player player;

        public StoreItem(Player player) {
            this.player = player;
        }
    }

    private class StoreItemAdapter extends BaseAdapter {

        private final Context mContext;
        private List<StoreItem> mStoreItems;
        private Map<String, Integer> mStoreItemsPositions;

        public StoreItemAdapter(Context context) {
            mContext = context;
            mStoreItems = new ArrayList<>();
            mStoreItemsPositions = new HashMap<>();
        }

        public void loadStoreItems(List<StoreItem> storeItems, Map<String, Integer> storeItemsPositions) {
            mStoreItems = storeItems;
            mStoreItemsPositions = storeItemsPositions;

            notifyDataSetChanged();
        }

        public void handleNewTransaction(String playerId) {
            Log.d(TAG, "handleNewTransaction - playerId: " + playerId);
            int position = getStoreItemPositionById(playerId);
            mStoreItems.remove(position);

            // Recompute mStoreItemsPositions (lazy approach)
            mStoreItemsPositions = new HashMap<>();
            Log.d(TAG, "Players in the store:");
            for (int i = 0; i < mStoreItems.size(); i++) {
                StoreItem storeItem = mStoreItems.get(i);
                Log.d(TAG, storeItem.player.name + " (" + storeItem.player.id + ")");
                mStoreItemsPositions.put(storeItem.player.id, i);
            }

            notifyDataSetChanged();
        }

        public Integer getStoreItemPositionById(String playerId) {
            return mStoreItemsPositions.get(playerId);
        }

        public StoreItem getStoreItemById(String playerId) {
            Integer position = getStoreItemPositionById(playerId);

            if (position == null) {
                return null;
            }

            return (StoreItem) getItem(position);
        }

        @Override
        public int getCount() {
            return mStoreItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mStoreItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;
            StoreItem storeItem = (StoreItem) getItem(position);

            if (convertView == null) {
                view = inflater.inflate(R.layout.store_item, null);
            } else {
                view = convertView;
            }

            ImageView runImage = (ImageView) view.findViewById(R.id.runImage);
            ImageView sleepImage = (ImageView) view.findViewById(R.id.sleepImage);
            ImageView itemBackground = (ImageView) view.findViewById(R.id.itemBackground);
            TextView name = (TextView) view.findViewById(R.id.name);
            TextView price = (TextView) view.findViewById(R.id.price);

            name.setTypeface(billy);
            price.setTypeface(billy);

            // Set player's name and currency
            name.setText(storeItem.player.name);
            DecimalFormat decimalFormat = new DecimalFormat();
            price.setText(decimalFormat.format(storeItem.player.price));

            if (storeItem.player.isMoving) {
                runImage.setVisibility(View.VISIBLE);
                sleepImage.setVisibility(View.GONE);
            } else {
                runImage.setVisibility(View.GONE);
                sleepImage.setVisibility(View.VISIBLE);
            }

            runImage.setBackgroundResource(R.drawable.run);
            AnimationDrawable runAnimation = (AnimationDrawable) runImage.getBackground();
            runAnimation.start();
            sleepImage.setBackgroundResource(R.drawable.sleep);
            AnimationDrawable sleepAnimation = (AnimationDrawable) sleepImage.getBackground();
            sleepAnimation.start();

            // Set background color
            int c = Color.parseColor(storeItem.player.color);
            ColorFilter cf = new PorterDuffColorFilter(Color.rgb(Color.red(c), Color.green(c), Color.blue(c)), PorterDuff.Mode.MULTIPLY);
            itemBackground.setColorFilter(cf);

            // Decrease alpha if current player hasn't sufficient funds to buy the player
            if (mBalance < storeItem.player.price) {
                Log.d(TAG, "Player " + storeItem.player.name + "'s (" + storeItem.player.id + ") price: " + storeItem.player.price + " " + getString(R.string.currency));
                view.setAlpha(0.5f);
            } else {
                view.setAlpha(1f);
            }

            return view;
        }
    }

    private static class SubscribeToTopic extends AsyncTask<Void, Void, Void> {
        private final Context mContext;
        private final String mToken;
        private final String mPlayerId;

        public SubscribeToTopic(Context context, String token, String playerId) {
            mContext = context;
            mToken = token;
            mPlayerId = playerId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            GcmPubSub pubSub = GcmPubSub.getInstance(mContext);
            if (mToken == null) {
                Log.e(TAG, "GCM token is null, skipping subscribe to player...");
                return null;
            }

            try {
                pubSub.subscribe(mToken, "/topics/player-" + mPlayerId, null);
            } catch (IOException e) {
                Log.e(TAG, "Can't subscribe to topic player-" + mPlayerId, e);
            }

            return null;
        }
    }
}
