package com.yeahright.hashirehamusutaa;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class StepCounterService extends Service implements SensorEventListener {

    private static final String TAG = "StepCounterService";
    // Intent action for 401 errors
    public static final String UNAUTHORIZED = "com.yeahright.hamasutaa.UNAUTHORIZED";
    // Max batch latency is specified in microseconds
    private static final int BATCH_LATENCY_10s = 10 * 1000 * 1000;
    // Milliseconds between mStanding runnable delyed executions
    private static final long STANDING_DELAY = 10 * 1000;
    // Number of steps to take to achieve the daily goal
    public static final int DAILY_STEPS_GOAL = 10000;

    private SensorManager mSensorManager = null;

    // Value of the step counter mSensor when the listener was registered.
    // (Total steps are calculated from this value.)
    private int mCounterSteps = 0;
    // Steps counted by the step counter previously. Used to keep counter consistent across rotation
    // changes
    private int mPreviousCounterSteps = 0;
    // Last aggregated steps count sent to server
    private int mRemoteTotalSteps = -1;

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new StepCounterBinder();

    private boolean isResetDailyStepsReceiverRegistered = false;
    private boolean isBonusEventReceiverRegistered = false;
    private StepCounterListener mStepCounterListener = null;
    private PlayerListener mPlayerListener = null;
    private Handler mStanding = new Handler();

    private SharedPreferences mSharedPreferences;
    private String mPlayerId;
    private String mToken;
    private Player mPlayer;
    private boolean isPlayerLoaded = false;
    private boolean isDailyStepsGoalAchieved;
    private List<String> mPlayerIds = new ArrayList<>();

    private BroadcastReceiver mResetDailyStepsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Resetting daily steps counter");

            mPlayer.dailySteps = 0;
            isDailyStepsGoalAchieved = false;

            // Save updated values in shared preferences
            mSharedPreferences.edit().putInt(HamusutaaPreferences.DAILY_STEPS, mPlayer.dailySteps).apply();
            mSharedPreferences.edit().putBoolean(HamusutaaPreferences.DAILY_STEPS_GOAL_ACHIEVED, isDailyStepsGoalAchieved).apply();

            if (mStepCounterListener != null) {
                mStepCounterListener.onPlayerChange(mPlayer);
            }
        }
    };

    private BroadcastReceiver mBonusEventBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Production bonus event");

            int bonusAmount = intent.getIntExtra(HamusutaaPreferences.BONUS_AMOUNT, 0);
            Log.d(TAG, "Intent extra " + HamusutaaPreferences.BONUS_AMOUNT + ": " + bonusAmount);
            mPlayer.balance += bonusAmount;
            mSharedPreferences.edit().putInt(HamusutaaPreferences.BALANCE, mPlayer.balance).apply();

            if (mStepCounterListener != null) {
                mStepCounterListener.onPlayerChange(mPlayer);
            }
        }
    };

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class StepCounterBinder extends Binder {
        StepCounterService getService() {
            return StepCounterService.this;
        }
    }

    public interface StepCounterListener {
        // On player changes run this function
        void onPlayerChange(Player player);
        // After STANDING_DELAY seconds that there are not stepn number changes
        void onStopMoving();
    }

    public interface PlayerListener {
        void onPlayerLoaded(Player player, List<String> playerIds);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Start command received...");

        // Don't try to restart the service if there are no user info
        if (mPlayerId == null) {
            return START_NOT_STICKY;
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Creating new service!");

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mPlayerId = mSharedPreferences.getString(HamusutaaPreferences.PLAYER_ID, null);

        if (mPlayerId == null) {
            // Stop the service if there's no user info
            Log.w(TAG, "Missing player info, stopping service.");
            stopSelf();
            return;
        }

        mToken = mSharedPreferences.getString(HamusutaaPreferences.TOKEN, null);
        int totalSteps = mSharedPreferences.getInt(HamusutaaPreferences.TOTAL_STEPS, 0);
        int dailySteps = mSharedPreferences.getInt(HamusutaaPreferences.DAILY_STEPS, 0);
        int balance = mSharedPreferences.getInt(HamusutaaPreferences.BALANCE, 0);
        isDailyStepsGoalAchieved = mSharedPreferences.getBoolean(HamusutaaPreferences.DAILY_STEPS_GOAL_ACHIEVED, false);

        mPlayer = new Player(mPlayerId, "Test", "#00ccff", 0, false, totalSteps, dailySteps, balance);

        // Set recurring task to reset daily steps
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Log.d(TAG, String.format("Setting repeating alarm for: %tc", calendar));
        Log.d(TAG, String.format("Calendar time in millis: %d", calendar.getTimeInMillis()));
        Intent intent = new Intent(HamusutaaPreferences.RESET_DAILY_STEP_COUNTER);
        PendingIntent resetDailyStepsIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, resetDailyStepsIntent);

        fetchPlayer();
        registerEventListener();
        registerResetDailyStepsReceiver();
        registerBonusEventReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Destroying service");

        // Save the last value of steps
        mSharedPreferences.edit().putInt(HamusutaaPreferences.TOTAL_STEPS, mPlayer.totalSteps).apply();
        mSharedPreferences.edit().putInt(HamusutaaPreferences.DAILY_STEPS, mPlayer.dailySteps).apply();
        mSharedPreferences.edit().putInt(HamusutaaPreferences.BALANCE, mPlayer.balance).apply();
        mSharedPreferences.edit().putBoolean(HamusutaaPreferences.DAILY_STEPS_GOAL_ACHIEVED, isDailyStepsGoalAchieved).apply();

        unregisterEventListener();
        unregisterResetDailyStepsReceiver();
        unregisterBonusEventReceiver();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // do nothing
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            // Remove the current standing thread
            mStanding.removeCallbacksAndMessages(null);

            // A step counter event contains the total number of steps since the listener
            // was first registered. We need to keep track of this initial value to calculate the
            // number of steps taken, as the first value a listener receives is undefined.
            if (mCounterSteps < 1) {
                // initial value
                mCounterSteps = (int) event.values[0];
            }

            // Calculate steps taken based on first counter value received.
            int stepsDelta = (int) event.values[0] - mCounterSteps - mPreviousCounterSteps;

            if (stepsDelta > 0) {
                Log.d(TAG, "STEP COUNTER EVENTS SIZE: " + event.values.length);
                Log.d(TAG, "STEP COUNTER EVENT VALUE: " + (int) event.values[0]);
                Log.d(TAG, "mCounterSteps: " + mCounterSteps);
                Log.d(TAG, "mPlayer.totalSteps: " + mPlayer.totalSteps);
                Log.d(TAG, "mPlayer.dailySteps: " + mPlayer.dailySteps);
                Log.d(TAG, "mPlayer.balance: " + mPlayer.balance);
                Log.d(TAG, "mPreviousCounterSteps: " + mPreviousCounterSteps);
                Log.d(TAG, "stepsDelta: " + stepsDelta);

                mPlayer.totalSteps += stepsDelta;
                mPlayer.dailySteps += stepsDelta;
                mPlayer.balance += stepsDelta;
                mPreviousCounterSteps += stepsDelta;

                Log.d(TAG, "UPDATED mPlayer.totalSteps: " + mPlayer.totalSteps);
                Log.d(TAG, "UPDATED mPlayer.dailySteps: " + mPlayer.dailySteps);
                Log.d(TAG, "UPDATED mPlayer.balance: " + mPlayer.balance);
                Log.d(TAG, "UPDATED mPreviousCounterSteps: " + mPreviousCounterSteps);

                // Check for the daily goal achievement
                if (!isDailyStepsGoalAchieved && mPlayer.dailySteps > DAILY_STEPS_GOAL) {
                    isDailyStepsGoalAchieved = true;
                    mSharedPreferences.edit().putBoolean(HamusutaaPreferences.DAILY_STEPS_GOAL_ACHIEVED, isDailyStepsGoalAchieved).apply();
                    HamusutaaNotification notificationHelper = new HamusutaaNotification(this);
                    notificationHelper.sendDailyStepsGoalAchieved();
                }

                // Save the last value of steps
                mSharedPreferences.edit().putInt(HamusutaaPreferences.TOTAL_STEPS, mPlayer.totalSteps).apply();
                mSharedPreferences.edit().putInt(HamusutaaPreferences.DAILY_STEPS, mPlayer.dailySteps).apply();
                mSharedPreferences.edit().putInt(HamusutaaPreferences.BALANCE, mPlayer.balance).apply();

                if (mStepCounterListener != null) {
                    mStepCounterListener.onPlayerChange(mPlayer);
                }

                if (!mPlayer.isMoving) {
                    Log.i(TAG, "MOVING");
                    Log.d(TAG, "mRemoteTotalSteps: " + mRemoteTotalSteps);
                    // If player was standing reset its status and send a HTTP request to
                    // update the server with the new relative number of steps
                    mPlayer.isMoving = true;

                    sendMeasurement();
                }

                // Delay a new standing thread to notify of nochanges after STANDING_DELAY seconds
                // after the last sensor change
                mStanding.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "NOT MOVING");
                        Log.d(TAG, "mRemoteTotalSteps: " + mRemoteTotalSteps);
                        // Update player status to "standing" and send HTTP request to update
                        // the server status
                        mPlayer.isMoving = false;

                        sendMeasurement();

                        if (mStepCounterListener != null) {
                            mStepCounterListener.onStopMoving();
                        }
                    }
                }, STANDING_DELAY);
            }
        } else {
            Log.w(TAG, "Sensor event not supported");
        }
    }

    private void sendMeasurement() {
        // Don't send the measurement if mRemoteTotalSteps has an inconsistent value
        if (mRemoteTotalSteps > -1) {
            final int newRemoteTotalSteps = mPlayer.totalSteps;
            HamusutaaClient.postMeasurement(mToken, mPlayer.totalSteps - mRemoteTotalSteps, mPlayer.isMoving, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Log.i(TAG, "Measurement sent successfully");
                    mRemoteTotalSteps = newRemoteTotalSteps;
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    String bodyString = null;
                    if (responseBody != null) {
                        bodyString = new String(responseBody);
                    }
                    Log.e(TAG, "Measurement error: " + statusCode + ", body: " + bodyString, error);

                    // Remove stored preferences and open registration intent if the token is not valid anymore
                    if (statusCode == 401) {
                        mSharedPreferences.edit().clear().apply();

                        Intent intent = new Intent(UNAUTHORIZED);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }
                }
            });
        }
    }

    // Request player info
    public void fetchPlayer() {
        if (!isPlayerLoaded) {
            HamusutaaClient.getMe(mToken, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                    Log.i(TAG, "Got my info successfully: " + responseBody.toString());
                    isPlayerLoaded = true;
                    try {
                        mRemoteTotalSteps = responseBody.getInt("totalSteps");
                        int totalSteps = mPlayer.totalSteps;
                        int dailySteps = mPlayer.dailySteps;
                        boolean isMoving = mPlayer.isMoving;
                        mPlayer = new Player(responseBody);
                        // Override player's steps and status
                        // Note: let's avoid to override player's balance, but be aware that is
                        // a trade off to get server updated value in case of bonuses
                        mPlayer.totalSteps = totalSteps;
                        mPlayer.dailySteps = dailySteps;
                        mPlayer.isMoving = isMoving;
                        JSONArray players = responseBody.getJSONArray("players");
                        for (int i = 0; i < players.length(); i++) {
                            Player player = new Player(players.getJSONObject(i));
                            mPlayerIds.add(player.id);
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "Error trying to get player's info", e);
                    }

                    if (mPlayerListener != null) {
                        mPlayerListener.onPlayerLoaded(mPlayer, mPlayerIds);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseBody) {
                    Log.e(TAG, "GET /me error: " + statusCode + ", body: " + responseBody, error);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                    Log.e(TAG, "GET /me error: " + statusCode + ", body: " + responseBody, error);

                    // Remove stored preferences and open registration intent if the token is not valid anymore
                    if (statusCode == 401) {
                        mSharedPreferences.edit().clear().apply();

                        Intent intent = new Intent(UNAUTHORIZED);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }
                }
            });
        } else {
            if (mPlayerListener != null) {
                mPlayerListener.onPlayerLoaded(mPlayer, mPlayerIds);
            }
        }
    }

    /**
     * Register a {@link android.hardware.SensorEventListener} for the mSensor and max batch delay.
     * The maximum batch delay specifies the maximum duration in microseconds for which subsequent
     * mSensor events can be temporarily stored by the mSensor before they are delivered to the
     * registered SensorEventListener. A larger delay allows the system to handle mSensor events more
     * efficiently, allowing the system to switch to a lower power state while the mSensor is
     * capturing events. Once the max delay is reached, all stored events are delivered to the
     * registered listener. Note that this value only specifies the maximum delay, the listener may
     * receive events quicker. A delay of 0 disables batch mode and registers the listener in
     * continuous mode.
     * The optimium batch delay depends on the application. For example, a delay of 5 seconds or
     * higher may be appropriate for an  application that does not update the UI in real time.
     */
    private void registerEventListener() {
        // Get the default sensor for the sensor type from the SensorManager
        mSensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);
        Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        // Register the listener for this sensor in batch mode.
        // If the max delay is 0, events will be delivered in continuous mode without batching.
        final boolean batchMode = mSensorManager.registerListener(
                this, sensor, SensorManager.SENSOR_DELAY_NORMAL, BATCH_LATENCY_10s);

        if (!batchMode) {
            Log.w(TAG, "Could not register sensor listener in batch mode, " +
                    "falling back to continuous mode.");
        } else {
            Log.i(TAG, "Listener registered successfully!");
        }
    }

    // Unregisters the mSensor listener if it is registered.
    private void unregisterEventListener() {
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
            Log.i(TAG, "Sensor listener unregistered.");
        }
    }

    public void setStepCounterListener(StepCounterListener listener) {
        mStepCounterListener = listener;
    }

    public void setPlayerListener(PlayerListener playerListener) {
        mPlayerListener = playerListener;
    }

    // Register a broadcast receiver for the scheduled reset daily steps event.
    private void registerResetDailyStepsReceiver() {
        if(!isResetDailyStepsReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter(HamusutaaPreferences.RESET_DAILY_STEP_COUNTER);
            registerReceiver(mResetDailyStepsBroadcastReceiver, intentFilter);
            isResetDailyStepsReceiverRegistered = true;
        }
    }

    // Unregister a broadcast receiver for the scheduled reset daily steps event.
    private void unregisterResetDailyStepsReceiver() {
        unregisterReceiver(mResetDailyStepsBroadcastReceiver);
        isResetDailyStepsReceiverRegistered = false;
    }

    private void registerBonusEventReceiver() {
        if (!isBonusEventReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter(HamusutaaGcmListenerService.BONUS_EVENT);
            LocalBroadcastManager.getInstance(this).registerReceiver(mBonusEventBroadcastReceiver, intentFilter);
            isBonusEventReceiverRegistered = true;
        }
    }

    private void unregisterBonusEventReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBonusEventBroadcastReceiver);
        isBonusEventReceiverRegistered = false;
    }
}