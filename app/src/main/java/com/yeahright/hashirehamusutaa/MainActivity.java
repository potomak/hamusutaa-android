package com.yeahright.hashirehamusutaa;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private boolean isLocalBroadcastReceiverRegistered = false;
    private StepCounterService mStepCounterService;
    private boolean isStepCounterServiceBound = false;

    private ProgressDialog mRegistrationProgressBar;
    private TextView mTotalStepsTextView;
    private TextView mDailyStepsTextView;
    private TextView mBalanceTextView;
    private ImageView mItemBackground;
    private TextView mPriceTextView;
    private TextView mNameTextView;
    private View mTextFields;
    private View mHamsterImage;
    private ImageView mRunImage;
    private ImageView mSleepImage;
    private AnimationDrawable mRunAnimation;
    private AnimationDrawable mSleepAnimation;

    private String mPlayerId;
    private String mToken;
    private String mGcmToken;
    private List<String> mPlayerIds;
    private int mBalance;

    private void setHamsterStatus(boolean isMoving) {
        if (isMoving) {
            setRunningHamster();
        } else {
            setSleepingHamster();
        }
    }

    private void setRunningHamster() {
        mRunImage.setVisibility(View.VISIBLE);
        mSleepImage.setVisibility(View.GONE);
    }

    private void setSleepingHamster() {
        mRunImage.setVisibility(View.GONE);
        mSleepImage.setVisibility(View.VISIBLE);
    }

    private StepCounterService.StepCounterListener mStepCounterListener = new StepCounterService.StepCounterListener() {
        @Override
        public void onPlayerChange(Player player) {
            DecimalFormat decimalFormat = new DecimalFormat();
            mTotalStepsTextView.setText(decimalFormat.format(player.totalSteps));
            mDailyStepsTextView.setText(decimalFormat.format(player.dailySteps));
            mBalanceTextView.setText(decimalFormat.format(player.balance));
            setHamsterStatus(player.isMoving);
        }

        @Override
        public void onStopMoving() {
            setSleepingHamster();
        }
    };

    private StepCounterService.PlayerListener mPlayerListener = new StepCounterService.PlayerListener() {

        @Override
        public void onPlayerLoaded(Player player, List<String> playerIds) {
            DecimalFormat decimalFormat = new DecimalFormat();
            mTotalStepsTextView.setText(decimalFormat.format(player.totalSteps));
            mDailyStepsTextView.setText(decimalFormat.format(player.dailySteps));
            mBalanceTextView.setText(decimalFormat.format(player.balance));
            mPriceTextView.setText(decimalFormat.format(player.price));
            mNameTextView.setText(player.name);
            mTextFields.setVisibility(View.VISIBLE);

            mPlayerIds = playerIds;
            mBalance = player.balance;

            setHamsterStatus(player.isMoving);
            mHamsterImage.setVisibility(View.VISIBLE);

            // Set background color
            int c = Color.parseColor(player.color);
            ColorFilter cf = new PorterDuffColorFilter(Color.rgb(Color.red(c), Color.green(c), Color.blue(c)), PorterDuff.Mode.MULTIPLY);
            mItemBackground.setColorFilter(cf);
            mItemBackground.setVisibility(View.VISIBLE);

            if (checkPlayServices()) {
                Log.i(TAG, "Starting GCM registration service...");
                // Start IntentService to register this application with GCM.
                final Intent registrationIntent = new Intent(getApplicationContext(), RegistrationIntentService.class);

                ArrayList<String> topicPlayerIds = new ArrayList<>(playerIds);
                // Include current player's id to topics to follow
                topicPlayerIds.add(mPlayerId);

                registrationIntent.putExtra(HamusutaaPreferences.TOPIC_PLAYER_IDS, topicPlayerIds);
                startService(registrationIntent);
            } else {
                Log.e(TAG, "Play services not supported!");
            }

            mRegistrationProgressBar.dismiss();
        }
    };

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case HamusutaaGcmListenerService.REGISTRATION_COMPLETE:
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                    boolean sentToken = sharedPreferences.getBoolean(HamusutaaPreferences.SENT_TOKEN_TO_SERVER, false);
                    mGcmToken = sharedPreferences.getString(HamusutaaPreferences.GCM_TOKEN, null);
                    if (sentToken) {
                        Log.d(TAG, "Connected to GCM");
                    } else {
                        Log.e(TAG, "Error connecting to GCM");
                        Toast.makeText(context, getString(R.string.gcm_connection_error), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case StepCounterService.UNAUTHORIZED:
                    Intent registerIntent = new Intent(context, RegisterActivity.class);
                    startActivity(registerIntent);
                    finish();
                    break;
                default:
                    Log.e(TAG, "Skipping action: " + intent.getAction());
            }
        }
    };

    private ServiceConnection mStepCounterConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            Log.d(TAG, "StepCounterService connected!");
            isStepCounterServiceBound = true;
            mStepCounterService = ((StepCounterService.StepCounterBinder)service).getService();
            mStepCounterService.setStepCounterListener(mStepCounterListener);
            mStepCounterService.setPlayerListener(mPlayerListener);
            mStepCounterService.fetchPlayer();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            mStepCounterService = null;
            isStepCounterServiceBound = false;
            Log.w(TAG, "StepCounterService disconnected!");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mPlayerId = sharedPreferences.getString(HamusutaaPreferences.PLAYER_ID, null);
        mToken = sharedPreferences.getString(HamusutaaPreferences.TOKEN, null);

        // If the player is not registered let's open the RegisterActivity and skip
        // all the other steps
        if (mPlayerId == null) {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_main);

        ImageView powerPlantButton = (ImageView) findViewById(R.id.powerPlantButton);
        ImageView storeButton = (ImageView) findViewById(R.id.storeButton);
        mItemBackground = (ImageView) findViewById(R.id.itemBackground);
        mTotalStepsTextView = (TextView) findViewById(R.id.totalStepsTextView);
        mDailyStepsTextView = (TextView) findViewById(R.id.dailyStepsTextView);
        mBalanceTextView = (TextView) findViewById(R.id.balanceTextView);
        mPriceTextView = (TextView) findViewById(R.id.priceTextView);
        mNameTextView = (TextView) findViewById(R.id.nameTextView);
        mTextFields = findViewById(R.id.textFields);
        mHamsterImage = findViewById(R.id.hamsterImage);
        TextView totalStepsLabel = (TextView) findViewById(R.id.totalStepsLabel);
        TextView dailyStepsLabel = (TextView) findViewById(R.id.dailyStepsLabel);
        TextView balanceLabel = (TextView) findViewById(R.id.balanceLabel);
        TextView priceLabel = (TextView) findViewById(R.id.priceLabel);
        TextView powerPlantLabel = (TextView) findViewById(R.id.powerPlantLabel);
        TextView storeLabel = (TextView) findViewById(R.id.storeLabel);

        Typeface billy = Typeface.createFromAsset(getAssets(), "billy.ttf");
        mTotalStepsTextView.setTypeface(billy);
        mDailyStepsTextView.setTypeface(billy);
        mBalanceTextView.setTypeface(billy);
        mPriceTextView.setTypeface(billy);
        mNameTextView.setTypeface(billy);
        totalStepsLabel.setTypeface(billy);
        dailyStepsLabel.setTypeface(billy);
        balanceLabel.setTypeface(billy);
        priceLabel.setTypeface(billy);
        powerPlantLabel.setTypeface(billy);
        storeLabel.setTypeface(billy);

        mRunImage = (ImageView) findViewById(R.id.runImage);
        mRunImage.setBackgroundResource(R.drawable.run);
        mRunAnimation = (AnimationDrawable) mRunImage.getBackground();

        mSleepImage = (ImageView) findViewById(R.id.sleepImage);
        mSleepImage.setBackgroundResource(R.drawable.sleep);
        mSleepAnimation = (AnimationDrawable) mSleepImage.getBackground();

        powerPlantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PowerPlantActivity.class);
                intent.putExtra(HamusutaaPreferences.PLAYER_ID, mPlayerId);
                intent.putExtra(HamusutaaPreferences.TOKEN, mToken);
                intent.putExtra(HamusutaaPreferences.GCM_TOKEN, mGcmToken);
                intent.putExtra(HamusutaaPreferences.PLAYER_IDS, (ArrayList) mPlayerIds);
                intent.putExtra(HamusutaaPreferences.BALANCE, mBalance);
                startActivity(intent);
            }
        });

        storeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), StoreActivity.class);
                intent.putExtra(HamusutaaPreferences.PLAYER_ID, mPlayerId);
                intent.putExtra(HamusutaaPreferences.TOKEN, mToken);
                intent.putExtra(HamusutaaPreferences.GCM_TOKEN, mGcmToken);
                intent.putExtra(HamusutaaPreferences.PLAYER_IDS, (ArrayList) mPlayerIds);
                intent.putExtra(HamusutaaPreferences.BALANCE, mBalance);
                startActivity(intent);
            }
        });

        mRegistrationProgressBar = ProgressDialog.show(this, "", "Loading. Please wait...", true);
        mRegistrationProgressBar.setContentView(R.layout.progress_dialog);

        // Show the registration card if the hardware is supported, show an error otherwise
        if (isKitkatWithStepSensor()) {
            Log.i(TAG, "Step sensor supported");
            startStepCounterService();
        } else {
            Log.e(TAG, "Step sensor not supported!");
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mRunAnimation.start();
        mSleepAnimation.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindStepCounterService();
        registerLocalBroadcastReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver();
        unbindStepCounterService();
    }

    // Register a local broadcast receiver (for the GSM registration event and unauthorized errors).
    private void registerLocalBroadcastReceiver() {
        if(!isLocalBroadcastReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(HamusutaaGcmListenerService.REGISTRATION_COMPLETE);
            intentFilter.addAction(StepCounterService.UNAUTHORIZED);
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, intentFilter);
            isLocalBroadcastReceiverRegistered = true;
        }
    }

    // Unregister a broadcast receiver for the GSM registration event.
    private void unregisterReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isLocalBroadcastReceiverRegistered = false;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Returns true if this device is supported. It needs to be running Android KitKat (4.4) or
     * higher and has a step counter and step detector sensor.
     * This check is useful when an app provides an alternative implementation or different
     * functionality if the step sensors are not available or this code runs on a platform version
     * below Android KitKat. If this functionality is required, then the minSDK parameter should
     * be specified appropriately in the AndroidManifest.
     *
     * @return True iff the device can run this sample
     */
    private boolean isKitkatWithStepSensor() {
        // Require at least Android KitKat
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        // Check that the device supports the step counter and detector sensors
        PackageManager packageManager = getPackageManager();
        return currentApiVersion >= android.os.Build.VERSION_CODES.KITKAT
                && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER);
    }

    // Start the StepCounterService
    private void startStepCounterService() {
        Log.i(TAG, "startStepCounterService...");
        Intent stepCounterIntent = new Intent(getApplicationContext(), StepCounterService.class);
        startService(stepCounterIntent);
    }

    private void bindStepCounterService() {
        // Establish a connection with the service.  We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).
        Intent intent = new Intent(this, StepCounterService.class);
        bindService(intent, mStepCounterConnection, Context.BIND_AUTO_CREATE);
    }

    private void unbindStepCounterService() {
        if (isStepCounterServiceBound) {
            // Detach our existing connection.
            unbindService(mStepCounterConnection);
        }
    }
}
