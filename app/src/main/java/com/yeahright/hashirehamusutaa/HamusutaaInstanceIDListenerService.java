package com.yeahright.hashirehamusutaa;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class HamusutaaInstanceIDListenerService extends InstanceIDListenerService {

    private static final String TAG = "HamusutaaInstanceIDLS";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        // Start IntentService to register this application with GCM.
        final Intent intent = new Intent(this, RegistrationIntentService.class);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String playerId = sharedPreferences.getString(HamusutaaPreferences.PLAYER_ID, null);
        String token = sharedPreferences.getString(HamusutaaPreferences.TOKEN, null);

        final ArrayList<String> topicPlayerIds = new ArrayList<>();
        topicPlayerIds.add(playerId);

        // Request player info
        HamusutaaClient.getMe(token, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                Log.i(TAG, "Got my info successfully: " + responseBody.toString());

                try {
                    JSONArray players = responseBody.getJSONArray("players");
                    for (int i = 0; i < players.length(); i++) {
                        String id = players.getJSONObject(i).getString("id");
                        topicPlayerIds.add(id);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "Error trying to get player's info", e);
                }

                intent.putExtra(HamusutaaPreferences.TOPIC_PLAYER_IDS, topicPlayerIds);
                startService(intent);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseBody) {
                Log.e(TAG, "GET /me error: " + statusCode + ", body: " + responseBody, error);
            }
        });
    }
    // [END refresh_token]
}