package com.yeahright.hashirehamusutaa;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;

/**
 * A register screen that offers registration.
 */
public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";

    // Keep track of the register request to ensure we can cancel it if requested.
    private boolean mIsRegistering = false;

    // UI references.
    private EditText mNameView;
    private View mRegisterFormView;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mNameView = (EditText) findViewById(R.id.name);
        mRegisterFormView = findViewById(R.id.registerForm);
        Button registerButton = (Button) findViewById(R.id.registerButton);

        Typeface billy = Typeface.createFromAsset(getAssets(), "billy.ttf");
        mNameView.setTypeface(billy);
        registerButton.setTypeface(billy);

        registerButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mIsRegistering) {
            return;
        }

        // Reset errors.
        mNameView.setError(null);

        // Store values at the time of the registration attempt.
        String name = mNameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid name.
        if (TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        } else if (!isNameValid(name)) {
            mNameView.setError(getString(R.string.error_invalid_name));
            focusView = mNameView;
            cancel = true;
        }

        // Choose random color
        Random rnd = new Random();
        int r = rnd.nextInt(256);
        int g = rnd.nextInt(256);
        int b = rnd.nextInt(256);
        String color = "#" + String.format("%02X", r) + String.format("%02X", g) + String.format("%02X", b);

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a tile task to
            // perform the user login attempt.
            showProgress(true);
            String timezone = TimeZone.getDefault().getID();
            HamusutaaClient.postPlayer(name, color, timezone, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                    Log.i(TAG, "POST player success: " + responseBody.toString());
                    try {
                        String playerId = responseBody.getString("playerId");
                        String token = responseBody.getString("token");

                        // Save playerId and token in the preferences
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        sharedPreferences.edit()
                                .putString(HamusutaaPreferences.PLAYER_ID, playerId)
                                .putString(HamusutaaPreferences.TOKEN, token)
                                .apply();
                    } catch (JSONException e) {
                        Log.e(TAG, "Error trying to read response", e);
                    }

                    // Start the main activity
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseBody) {
                    Log.e(TAG, "POST /players error: " + statusCode + ", body: " + responseBody, error);
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    showProgress(false);
                }
            });
        }
    }

    private boolean isNameValid(String name) {
        return name.length() <= 64;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegisterFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }

        if (show) {
            mProgressDialog = ProgressDialog.show(this, "", "Loading. Please wait...", true);
            mProgressDialog.setContentView(R.layout.progress_dialog);
        } else {
            mProgressDialog.dismiss();
        }
    }
}

