package com.yeahright.hashirehamusutaa;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

public class HamusutaaClient {
    private static final String TAG = "HamusutaaClient";
    private static final String BASE_URL = "https://hashire-hamusutaa.appspot.com";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void getMe(String token, JsonHttpResponseHandler responseHandler) {
        Log.i(TAG, "GET /me");
        Header[] headers = new Header[]{new BasicHeader("X-Auth-Token", token)};
        client.get(null, getAbsoluteUrl("/me"), headers, null, responseHandler);
    }

    public static void getPlayer(String playerId, JsonHttpResponseHandler responseHandler) {
        Log.i(TAG, "GET /players/" + playerId);
        client.get(getAbsoluteUrl("/players/" + playerId), responseHandler);
    }

    public static void getPlayers(Integer maxPrice, JsonHttpResponseHandler responseHandler) {
        String url = getAbsoluteUrl("/players/?maxPrice=" + maxPrice);
        Log.i(TAG, "GET " + url + " - maxPrice: " + maxPrice);
        client.get(url, responseHandler);
    }

    public static void postPlayer(String username, String color, String timezone, JsonHttpResponseHandler responseHandler) {
        String url = getAbsoluteUrl("/players/");
        Log.i(TAG, "POST " + url + " - username: " + username);
        JSONObject player = new JSONObject();
        try {
            player.put("name", username);
            player.put("color", color);
            player.put("timezone", timezone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringEntity entity = null;
        try {
            entity = new StringEntity(player.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "BODY: " + player);
        client.post(null, url, entity, "application/json", responseHandler);
    }

    public static void postMeasurement(String token, int steps, boolean isMoving, AsyncHttpResponseHandler responseHandler) {
        String url = getAbsoluteUrl("/measurements");
        Log.i(TAG, "POST " + url + " - steps: " + steps + ", isMoving: " + isMoving);
        Header[] headers = new Header[]{new BasicHeader("X-Auth-Token", token)};
        JSONObject measurement = new JSONObject();
        try {
            measurement.put("steps", steps);
            measurement.put("isMoving", isMoving);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringEntity entity = null;
        try {
            entity = new StringEntity(measurement.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "BODY: " + measurement);
        client.post(null, url, headers, entity, "application/json", responseHandler);
    }

    public static void postBuy(String playerId, String token, AsyncHttpResponseHandler responseHandler) {
        String url = getAbsoluteUrl("/players/" + playerId + "/buy");
        Log.i(TAG, "POST " + url);
        Header[] headers = new Header[]{new BasicHeader("X-Auth-Token", token)};
        StringEntity entity = null;
        client.post(null, url, headers, entity, "application/json", responseHandler);
    }

    public static void postPing(String playerId, String token, AsyncHttpResponseHandler responseHandler) {
        String url = getAbsoluteUrl("/players/" + playerId + "/ping");
        Log.i(TAG, "POST " + url);
        Header[] headers = new Header[]{new BasicHeader("X-Auth-Token", token)};
        StringEntity entity = null;
        client.post(null, url, headers, entity, "application/json", responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
