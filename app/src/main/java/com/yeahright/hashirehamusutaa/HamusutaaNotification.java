package com.yeahright.hashirehamusutaa;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import java.text.DecimalFormat;

public class HamusutaaNotification {

    private final Context mContext;

    public HamusutaaNotification(Context context) {
        mContext = context;
    }

    public void sendDailyStepsGoalAchieved() {
        DecimalFormat decimalFormat = new DecimalFormat();
        String title = mContext.getString(R.string.daily_steps_goal_achieved_title);
        String message = mContext.getString(R.string.daily_steps_goal_achieved_message);
        sendNotification(title, String.format(message, decimalFormat.format(StepCounterService.DAILY_STEPS_GOAL)));
    }

    public void sendBonusNotification(int bonusAmount) {
        DecimalFormat decimalFormat = new DecimalFormat();
        String currency = mContext.getString(R.string.currency);
        String title = mContext.getString(R.string.bonus_notification_title);
        String message = mContext.getString(R.string.bonus_notification_message);
        sendNotification(title, String.format(message, decimalFormat.format(bonusAmount), currency));
    }

    public void sendPingNotification(String name) {
        String title = mContext.getString(R.string.ping_notification_title);
        String message = mContext.getString(R.string.ping_notification_message);
        sendNotification(String.format(title, name), message);
    }

    /**
     * Create and show a simple notification.
     *
     * @param title Notification title.
     * @param message Notification message.
     */
    private void sendNotification(String title, String message) {
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_stat_hamusuta)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
