package com.yeahright.hashirehamusutaa;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HamusutaaGcmListenerService extends GcmListenerService {

    private static final String TAG = "HamusutaaGcmLLS";

    public static final String MEASUREMENT_EVENT = "com.yeahright.hamasutaa.MEASUREMENT";
    public static final String PING_EVENT = "com.yeahright.hamasutaa.PING";
    public static final String BONUS_EVENT = "com.yeahright.hamasutaa.BONUS";
    public static final String REGISTRATION_COMPLETE = "com.yeahright.hamasutaa.REGISTRATION_COMPLETE";

    private LocalBroadcastManager mBroadcaster;
    private String mPlayerId;

    @Override
    public void onCreate() {
        super.onCreate();
        mBroadcaster = LocalBroadcastManager.getInstance(this);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mPlayerId = sharedPreferences.getString(HamusutaaPreferences.PLAYER_ID, null);
    }

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String event = data.getString("event");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Event: " + event);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
            String pattern = "/topics/player-(\\d+)";

            // Create a Pattern object
            Pattern r = Pattern.compile(pattern);

            // Now create matcher object.
            Matcher m = r.matcher(from);
            if (m.find()) {
                Log.d(TAG, "Player id: " + m.group(1));
                String playerId = m.group(1);
                handlePlayerEvent(playerId, event, data);
            } else {
                Log.e(TAG, "NO MATCH FROM: " + from);
            }
        } else {
            // normal downstream message.
        }
    }

    private void handlePlayerEvent(String playerId, String event, Bundle data) {
        Intent intent;
        HamusutaaNotification notificationHelper = new HamusutaaNotification(this);

        switch (event) {
            case MEASUREMENT_EVENT:
                Log.d(TAG, "New measurement event");
                intent = new Intent(MEASUREMENT_EVENT);
                intent.putExtra(HamusutaaPreferences.PLAYER_ID, playerId);
                Log.d(TAG, "playerId: " + playerId);
                String isMovingString = data.getString(HamusutaaPreferences.IS_MOVING);
                Log.d(TAG, "isMovingString: " + isMovingString);
                boolean isMoving = Boolean.valueOf(isMovingString);
                Log.d(TAG, "isMoving: " + isMoving);
                intent.putExtra(HamusutaaPreferences.IS_MOVING, isMoving);
                mBroadcaster.sendBroadcast(intent);
                break;
            case PING_EVENT:
                Log.d(TAG, "New ping event");
                if (mPlayerId.equals(playerId)) {
                    String name = data.getString(HamusutaaPreferences.NAME);
                    notificationHelper.sendPingNotification(name);
                }
                break;
            case BONUS_EVENT:
                Log.d(TAG, "New bonus event");
                if (mPlayerId.equals(playerId)) {
                    String bonusAmountString = data.getString(HamusutaaPreferences.BONUS_AMOUNT);
                    Log.d(TAG, "Data " + HamusutaaPreferences.BONUS_AMOUNT + " (String): " + bonusAmountString);
                    int bonusAmount = Integer.valueOf(bonusAmountString);
                    Log.d(TAG, "Data " + HamusutaaPreferences.BONUS_AMOUNT + ": " + bonusAmount);
                    notificationHelper.sendBonusNotification(bonusAmount);
                    intent = new Intent(BONUS_EVENT);
                    intent.putExtra(HamusutaaPreferences.BONUS_AMOUNT, bonusAmount);
                    mBroadcaster.sendBroadcast(intent);
                }
                break;
            default:
                Log.i(TAG, "Unsupported event: " + event);
        }
    }
}