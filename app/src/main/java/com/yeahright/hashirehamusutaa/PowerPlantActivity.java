package com.yeahright.hashirehamusutaa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class PowerPlantActivity extends AppCompatActivity {

    private static final String TAG = "PowerPlantActivity";

    private BroadcastReceiver mReceiver;
    private PowerPlantItemAdapter mListAdapter;
    private Typeface billy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power_plant);

        final GridView gv = (GridView)findViewById(R.id.gridView);

        Intent intent = getIntent();
        final String token = intent.getStringExtra(HamusutaaPreferences.TOKEN);

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        final TextView introTextView = (TextView) findViewById(R.id.powerPlantIntroTextView);
        billy = Typeface.createFromAsset(getAssets(), "billy.ttf");
        introTextView.setTypeface(billy);

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String event = intent.getAction();

                switch (event) {
                    case HamusutaaGcmListenerService.MEASUREMENT_EVENT:
                        String playerId = intent.getStringExtra(HamusutaaPreferences.PLAYER_ID);
                        boolean isMoving = intent.getBooleanExtra(HamusutaaPreferences.IS_MOVING, false);
                        if (mListAdapter != null) {
                            mListAdapter.handleNewMeasurement(playerId, isMoving);
                        }
                        break;
                    default:
                        Log.i(TAG, "Unsupported event: " + event);
                }
            }
        };

        HamusutaaClient.getMe(token, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                Log.i(TAG, "Got my info: " + responseBody.toString());
                List<PowerPlantItem> powerPlantItems = new ArrayList<>();
                Map<String, Integer> powerPlantItemsPositions = new HashMap<>();
                try {
                    JSONArray players = responseBody.getJSONArray("players");
                    for (int i = 0; i < players.length(); i++) {
                        Player player = new Player(players.getJSONObject(i));
                        powerPlantItems.add(i, new PowerPlantItem(player));
                        powerPlantItemsPositions.put(player.id, i);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "Error creating power plant items list", e);
                }
                Log.d(TAG, "Power plant items: " + Arrays.toString(powerPlantItems.toArray()));
                Log.d(TAG, "Power plant items-positions: " + Arrays.toString(powerPlantItemsPositions.entrySet().toArray()));
                mListAdapter = new PowerPlantItemAdapter(getApplicationContext(), powerPlantItems, powerPlantItemsPositions);
                gv.setAdapter(mListAdapter);

                // Show intro if there are no power plant items
                if (powerPlantItems.size() == 0) {
                    introTextView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseBody) {
                Log.e(TAG, "GET /me error: " + statusCode + ", body: " + responseBody, error);
                Toast.makeText(getApplicationContext(), responseBody.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                Log.e(TAG, "GET /me error: " + statusCode + ", body: " + responseBody, error);

                // Remove stored preferences and open registration intent if the token is not valid anymore
                if (statusCode == 401) {
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().apply();

                    Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
                    startActivity(registerIntent);
                    finish();
                }
            }

            @Override
            public void onFinish() {
                progressBar.setVisibility(ProgressBar.GONE);
            }
        });

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final PowerPlantItem powerPlantItem = (PowerPlantItem) mListAdapter.getItem(position);
                HamusutaaClient.postPing(powerPlantItem.player.id, token, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Log.i(TAG, "Player " + powerPlantItem.player.name + " (" + powerPlantItem.player.id + ") pinged successfully");
                        mListAdapter.handlePing(powerPlantItem.player.id);
                        String message = getString(R.string.ping_success);
                        Toast.makeText(getApplicationContext(), String.format(message, powerPlantItem.player.name), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.e(TAG, "Error trying to ping player " + powerPlantItem.player.id, error);
                        String errorMessage;
                        if (responseBody != null) {
                            errorMessage = new String(responseBody);
                        } else {
                            errorMessage = error.getMessage();
                        }
                        Log.d(TAG, "Error message: " + errorMessage);
                        Toast.makeText(getApplicationContext(), errorMessage.trim(), Toast.LENGTH_SHORT).show();

                        // Remove stored preferences and open registration intent if the token is not valid anymore
                        if (statusCode == 401) {
                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().apply();

                            Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
                            startActivity(registerIntent);
                            finish();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(HamusutaaGcmListenerService.MEASUREMENT_EVENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentFilter);
    }

    @Override
    public void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        super.onStop();
    }

    private static class PowerPlantItem {
        public final Player player;

        public PowerPlantItem(Player player) {
            this.player = player;
        }
    }

    private class PowerPlantItemAdapter extends BaseAdapter {

        private final Context mContext;
        private final List<PowerPlantItem> mPowerPlantItems;
        private Map<String, Integer> mPowerPlantItemsPositions;

        public PowerPlantItemAdapter(Context context, List<PowerPlantItem> powerPlantItems, Map<String, Integer> powerPlantItemsPositions) {
            mContext = context;
            mPowerPlantItems = powerPlantItems;
            mPowerPlantItemsPositions = powerPlantItemsPositions;
        }

        public void handleNewMeasurement(String playerId, boolean isMoving) {
            PowerPlantItem powerPlantItem = getPowerPlantItemById(playerId);

            if (powerPlantItem == null) {
                Log.e(TAG, "Trying to handle measurement for player " + playerId + " that is not present");
                return;
            }

            powerPlantItem.player.isMoving = isMoving;
            notifyDataSetChanged();
        }

        public void handlePing(String playerId) {
            // TODO
            notifyDataSetChanged();
        }

        public Integer getPowerPlantItemPositionById(String playerId) {
            return mPowerPlantItemsPositions.get(playerId);
        }

        public PowerPlantItem getPowerPlantItemById(String playerId) {
            Integer position = getPowerPlantItemPositionById(playerId);

            if (position == null) {
                return null;
            }

            return (PowerPlantItem) getItem(position);
        }

        @Override
        public int getCount() {
            return mPowerPlantItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mPowerPlantItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;
            PowerPlantItem powerPlantItem = (PowerPlantItem) getItem(position);

            if (convertView == null) {
                view = inflater.inflate(R.layout.power_plant_item, null);
            } else {
                view = convertView;
            }

            ImageView runImage = (ImageView) view.findViewById(R.id.runImage);
            ImageView sleepImage = (ImageView) view.findViewById(R.id.sleepImage);
            ImageView itemBackground = (ImageView) view.findViewById(R.id.itemBackground);
            TextView name = (TextView) view.findViewById(R.id.name);

            name.setText(powerPlantItem.player.name);
            name.setTypeface(billy);

            if (powerPlantItem.player.isMoving) {
                runImage.setVisibility(View.VISIBLE);
                sleepImage.setVisibility(View.GONE);
            } else {
                runImage.setVisibility(View.GONE);
                sleepImage.setVisibility(View.VISIBLE);
            }

            runImage.setBackgroundResource(R.drawable.run);
            AnimationDrawable runAnimation = (AnimationDrawable) runImage.getBackground();
            runAnimation.start();
            sleepImage.setBackgroundResource(R.drawable.sleep);
            AnimationDrawable sleepAnimation = (AnimationDrawable) sleepImage.getBackground();
            sleepAnimation.start();

            // Set background color
            int c = Color.parseColor(powerPlantItem.player.color);
            ColorFilter cf = new PorterDuffColorFilter(Color.rgb(Color.red(c), Color.green(c), Color.blue(c)), PorterDuff.Mode.MULTIPLY);
            itemBackground.setColorFilter(cf);

            return view;
        }
    }
}
