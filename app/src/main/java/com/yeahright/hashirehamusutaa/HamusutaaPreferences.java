package com.yeahright.hashirehamusutaa;

import android.content.Context;

public class HamusutaaPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String RESET_DAILY_STEP_COUNTER = "resetDailyStepCounter";
    public static final String PLAYER_ID = "playerId";
    public static final String TOPIC_PLAYER_IDS = "topicPlayerIds";
    public static final String TOKEN = "token";
    public static final String TOTAL_STEPS = "totalSteps";
    public static final String DAILY_STEPS = "dailySteps";
    public static final String IS_MOVING = "isMoving";
    public static final String BALANCE = "balance";
    public static final String NAME = "name";
    public static final String PLAYER_IDS = "playerIds";
    public static final String GCM_TOKEN = "gcmToken";
    public static final String BONUS_AMOUNT = "bonusAmount";
    public static final String DAILY_STEPS_GOAL_ACHIEVED = "dailyStepsGoalAchieved";
}
